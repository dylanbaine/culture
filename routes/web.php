<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Mail\InviteUserToOrganization;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Controllers\Admin\SurveyController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\AppViewController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Admin\SurveySectionController;
use App\Http\Controllers\Admin\SurveyQuestionController;
use App\Http\Controllers\EmployeeCSVUploadController;
use App\Http\Controllers\Admin\SurveyOptionController;
use App\Http\Controllers\GetStartedController;

Route::get('/', [AppViewController::class, 'welcome'])->name('welcome');
Route::get('/app', [AppViewController::class, 'app'])->name('home');
Route::get('/take-survey/{survey}', [AppViewController::class, 'takeSurvey'])->name('take-survey');
Route::get('/admin', [AppViewController::class, 'admin'])->name('admin')->middleware(AdminMiddleware::class);

Route::get('/logout', [LoginController::class, 'logout']);
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('/register', [RegisterController::class, 'register']);

Route::get('/invite-email', function() {
    return new InviteUserToOrganization(User::find(1), 'test@test.com');
});

Route::post('/upload-employees-csv', EmployeeCSVUploadController::class);

Route::post('/get-started', GetStartedController::class)->name('get-started');

Route::prefix('/admin')->middleware(AdminMiddleware::class)->group(function() {
    Route::resource('/surveys', SurveyController::class)->names('surveys');
    Route::prefix('/surveys')->group(function() {
        Route::resource('/{surveyId}/sections', SurveySectionController::class)->names('survey-sections');
        Route::resource('/{surveyId}/sections/{sectionId}/options', SurveyOptionController::class)->names('survey-options');
        Route::resource('/{surveyId}/sections/{sectionId}/questions', SurveyQuestionController::class)->names('survey-questions');
    });
});
