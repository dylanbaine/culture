const mix = require('laravel-mix');
require('laravel-mix-artisan-serve');

mix.
    js('resources/js/app/index.js', 'public/js/app.js')
    .js('resources/js/site.js', 'public/js/site.js')
    .js('resources/js/take-survey/index.js', 'public/js/take-survey.js')
    .js('resources/js/admin/index.js', 'public/js/admin.js')

    .sass('resources/sass/app.scss', 'public/css')

    .serve()
    .disableNotifications()

    // https://laravel-mix.com/docs/5.0/browsersync
    .browserSync({
        proxy: 'http://localhost:8000',
        notify: false,
    })
