type Survey {
    id: ID!
    name: String!
    sections: [SurveySection]! @hasMany
}

type UserSurvey {
    id: ID!
    user: User
    survey: Survey!
}

type SurveyQuestionOption {
    id: ID!
    option: String!
}

type SurveySection {
    id: ID!
    name: String!
    description: String
    options: [SurveyQuestionOption]! @hasMany
}

type SurveyQuestion {
    id: ID!
    question: String
    use_checkbox: Boolean
    section: SurveySection @belongsto
}

type SurveyState {
    currentQuestion: SurveyQuestion!
}

type SurveyQuestionAnswer {
    id: ID!
}

type SurveyFrequency {
    id: ID!
    name: String!
}

type SurveySchedule {
    id: ID!
    organization: Organization!
    survey: Survey!
    frequency: SurveyFrequency
}

type ScheduledSurvey {
    survey: Survey!
    frequency: SurveyFrequency!
}

input AnswerSurveyInput {
    questionId: ID!
    optionId: ID
    manualInput: String
    surveyId: ID!
}

input ScheduleSurveyInput {
    surveyId: ID!
    frequencyId: ID!
}

extend type Query {
    pendingSurveys: [Survey]!
    takenSurveys(limit: ID): [UserSurvey]!
    availableSurveys: [Survey]!
    sentSurveysCount: Int!
    takenSurveysCount: Int!
    inProgressSurveysCount: Int!
    availableSurveyFrequencies: [SurveyFrequency!]!
    currentSchedule: [ScheduledSurvey]!
}

extend type Mutation {
    answerSurvey(input: AnswerSurveyInput): SurveyQuestionAnswer @can(ability: "answer" find: "input.surveyId" model: "App\\Models\\Survey")
    sendSurveyToOrganization(surveyId: ID!): Survey!
    markSurveyAsFinished(surveyId: ID!): Survey!
    createScheduleSurvey(input: ScheduleSurveyInput): SurveySchedule!
    removeSurveySchedule(surveyId: ID!, frequencyId: ID!): Boolean
}
