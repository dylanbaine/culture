<?php

use App\Models\AdminUser;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        User::whereIn('email', [
            'dylan.baine@yahoo.com',
            'paolo.g.trulli@gmail.com'
        ])->get()
        ->each(function($user) {
            AdminUser::create([
                'user_id' => $user->id,
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        AdminUser::query()->delete();
    }
}
