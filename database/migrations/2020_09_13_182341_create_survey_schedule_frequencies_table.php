<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyScheduleFrequenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_schedule_frequencies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longText('description');
            $table->timestamps();
        });
        DB::table('survey_schedule_frequencies')->insert([
            [
                'name' => 'Yearly',
                'description' => 'Once a year.',
            ],
            [
                'name' => 'Semiyearly',
                'description' => 'Twice a year. (every 6 months)'
            ],
            [
                'name' => 'Quarterly',
                'description' => 'Four times a year. (every 3 months)'
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_schedule_frequencies');
    }
}
