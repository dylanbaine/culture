<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswerScoreValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_score_values', function (Blueprint $table) {
            $table->id();
            $table->integer('question_id')->index();
            $table->integer('option_id')->index();
            $table->integer('value');
            $table->enum('operator', ['-', '+'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_score_values');
    }
}
