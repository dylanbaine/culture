<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeEmployeeSurveyColumnInSurveyQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_question_answers', function (Blueprint $table) {
            $table->renameColumn('employee_survey_id', 'user_survey_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_question_answers', function (Blueprint $table) {
            $table->renameColumn('user_survey_id', 'employee_survey_id');
        });
    }
}
