<?php

use App\Models\AdminUser;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Seeder;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orgs = factory(Organization::class, 3)->create();
        foreach($orgs as $org) {
            $org->createDefaultRoles();
            $role = $org->roles()->whereName('admin')->first();
            $created = 0;
            while ($created < 100) {
                factory(User::class)->create([
                    'organization_id' => $org->id,
                    'role_id' => $created == 0 ? $role->id : $role->id + 1
                ]);
                $created++;
            }
        }
        AdminUser::create([
            'user_id' => 1,
        ]);
    }
}
