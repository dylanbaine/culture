<?php

use App\Models\Survey;
use App\Models\SurveyQuestion;
use App\Models\SurveyQuestionOption;
use App\Models\SurveySection;
use Illuminate\Database\Seeder;

class SurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Survey::query()->delete();
        SurveySection::query()->delete();
        SurveyQuestion::query()->delete();
        $surveys = factory(Survey::class, 3)->create();
        foreach($surveys as $survey) {
            $sections = factory(SurveySection::class, 2)->create([
                'survey_id' => $survey->id,
            ]);
            foreach($sections as $section) {
                factory(SurveyQuestionOption::class, rand(2, 4))->create([
                    'section_id' => $section->id,
                ]);
                factory(SurveyQuestion::class, rand(12, 16))->create([
                    'section_id' => $section->id
                ]);
            }
        }
    }
}
