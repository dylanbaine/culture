<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SurveyQuestionOption;
use Faker\Generator as Faker;

$factory->define(SurveyQuestionOption::class, function (Faker $faker) {
    return [
        'option' => $faker->sentence(2),
    ];
});
