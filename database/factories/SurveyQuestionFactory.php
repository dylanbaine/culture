<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SurveyQuestion;
use Faker\Generator as Faker;

$factory->define(SurveyQuestion::class, function (Faker $faker) {
    return [
        'question' => $faker->sentence(rand(15, 27))
    ];
});
