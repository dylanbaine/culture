<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SurveySection;
use Faker\Generator as Faker;

$factory->define(SurveySection::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph(2)
    ];
});
