<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if(config('app.env') == 'local')
        <script src="http://localhost:35729/livereload.js"></script>
    @endif

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script>
        window.state = {
            user: {!! \Auth::check() ? \Auth::user()->load('organization', 'role')->toJson() : 'null' !!},
            error: null,
            success: null,
        };
        @if (session('success'))
            window.state.success = "{{session('success')}}";
        @elseif(session('error'))
            window.state.error = "{{session('error')}}";
        @endif

        window.csrf = '{{csrf_token()}}';
    </script>
</head>
<body>
    <div id="app" v-cloak>
        <v-app>
            <v-snackbar
                v-if="notify"
                v-model="notify.showing"
                :color="notify.type || 'info'"
                multi-line>
                @{{ notify.message }}
                <template v-slot:action="{ attrs }">
                    <v-btn text
                    v-bind="attrs"
                    @click="notify.showing = false">
                    Close
                    </v-btn>
                </template>
            </v-snackbar>
            @yield('content')
            <footer>
                <v-container fluid>
                    &copy; {{date('Y')}} @if (branch() != 'master' )- {{branch() }} @endif
                </v-container>
            </footer>
        </v-app>
    </div>
    @stack('scripts')
</body>
</html>
