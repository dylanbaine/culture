@extends('layouts.main')

@push('scripts')
  <script src="{{ asset('js/app.js') }}" defer></script>
@endpush

@section('content')
  <app-sidebar></app-sidebar>
  <app-toolbar></app-toolbar>
  <v-main>
    <v-container fluid>
      <v-breadcrumbs
        v-if="$route.meta.breadcrumbs"
        :items="$route.meta.breadcrumbs"
      ></v-breadcrumbs>
      <router-view></router-view>
    </v-container>
  </v-main>
@endsection
