@extends('layouts.main')

@push('scripts')
    <script src="{{ asset('js/site.js') }}" defer></script>
@endpush
<div id="page--welcome">
    @section('content')
    @include('site.header')
    <v-main>
        <header id="welcome">
            <v-container>
                <h1>
                    Survey staff anonymously to understand your organization's culture.
                    <br>
                    Identify what matters to employees.
                </h1>
                <h2>
                    Our research backed surveys can highlight areas to improve on.
                </h2>
            </v-container>
        </header>
        <v-container>
            <section id="partners"></section>
            <section id="sign-up">
                @component('_includes.lead-cta-form', ['buttonText' => 'Get Started'])
                    <h2>Understand your company's culture.</h2>
                    <p>Fill out the form to get started.</p>
                @endcomponent
            </section>
            <section id="features">
                <h2>Features</h2>
                <ul>
                    <li>
                        <div>
                            <h3>Identify what keeps employees motivated</h3>
                            <p>
                                Understand where employees feel appreciated.
                                An appreciated employee is a motivated one.
                            </p>
                        </div>
                        <div class="img" style="background-image: url(/media/motivated.jpeg)"></div>
                    </li>
    
                    <li>
                        <div class="img" style="background-image: url(/media/dialogue.jpeg)"></div>
                        <div>
                            <h3>Encourage an open dialogue between all staff.</h3>
                            <p>
                                Providing employees with decision making responsibility can lead
                                to a better dialogue within your organization.
                            </p>
                        </div>
                    </li>
    
                    <li>
                        <div>
                            <h3>Identify areas that need attention or improvement.</h3>
                            <p>
                                By completing the research backed surveys, your organization
                                will have better understanding of weak areas and where to
                                focus on improving.
                            </p>
                        </div>
                        <div class="img" style="background-image: url(/media/identify.jpeg)"></div>
                    </li>
    
                    <li>
                        <div class="img" style="background-image: url(/media/understand.jpeg)"></div>
                        <div>
                            <h3>Understand how staff feel working in your organization.</h3>
                            <p>
                                A successfull and inviting workplace culture is a great place to work.
                                To determine how to reach that level of success, you first need to
                                understand your staff.
                            </p>
                        </div>
                    </li>
    
                    <li>
                        <div>
                            <h3>Recognize reasons behind staff turnover</h3>
                            <p>
                                Understanding why your organization is experiencing 
                                high turnover and actionable ways to retain 
                                employees for the long haul
                            </p>
                        </div>
                        <div class="img" style="background-image: url(/media/recognize.jpeg)"></div>
                    </li>
    
                    <li>
                        <div class="img" style="background-image: url(/media/value.jpeg)"></div>
                        <div>
                            <h3>Value training and provide employee advancement</h3>
                            <p>
                                Determine what training staff prefer in order to
                                promote a healthy career path
                                leading to motivated and engaged employees
                            </p>
                        </div>
                    </li>
    
                    <li>
                        <div>
                            <h3>Give your staff the power of contributions</h3>
                            <p>
                                Key findings in organizations suggest employees 
                                tend to enjoy their work when given the opportunity 
                                to contribute to tasks and projects
                            </p>
                        </div>
                        <div class="img" style="background-image: url(/media/give.jpeg)"></div>
                    </li>
    
                    <li>
                        <div class="img" style="background-image: url(/media/establish.jpeg)"></div>
                        <div>
                            <h3>Establish an early culture that resonates with employees</h3>
                            <p>
                                Research suggests culture is easier to change 
                                or implement when your organization is new 
                                or small in size. Now is the time to ensure your
                                culture will be implemented but also 
                                adapt as your company grows   
                            </p>
                        </div>
                    </li>  
    
                </ul>
            </section>
            <section id="sign-up-baseline">
                @component('_includes.lead-cta-form')
                    <h2>Find your organizations baseline culture.</h2>
                @endcomponent
            </section>
        </v-container>
    </v-main>
</div>
@endsection