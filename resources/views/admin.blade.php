@extends('layouts.main')

@push('scripts')
  <script src="{{ asset('js/admin.js') }}" defer></script>
@endpush

@section('content')
    <admin-sidebar></admin-sidebar>
    <v-main>
        <v-container fluid>
            <v-row>
                <v-col cols="4">
                    <a href="{{route('surveys.index')}}">
                        <v-card>
                            <v-card-title>Surveys</v-card-title>
                        </v-card>
                    </a>
                </v-col>
            </v-row>
        </v-container>
    </v-main>
@endsection
