@php
    $url = "/register?email={$email}";
    if ($name) {
        $url .= "&name={$name}";
    }
    if ($invitedBy) {
        $url .= "&organization={$invitedBy->organization_id}";
    }
@endphp

@component('mail::message')
# Hey, there!

@if ($invitedBy)
    {{$invitedBy->name}} has invited you to join {{config('app.name')}}.
@else
    Thanks for signing up for {{config('app.name')}}!
@endif

@component('mail::button', ['url' => url($url)])
    Set up your account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
