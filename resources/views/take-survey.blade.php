@extends('layouts.main')

@push('scripts')
    <script>
        window.survey = {!! $survey->toJson() !!}
    </script>
  <script src="{{ asset('js/take-survey.js') }}" defer></script>
@endpush

@section('content')
  <v-main>
    <v-container fluid>
      <router-view></router-view>
    </v-container>
  </v-main>
@endsection
