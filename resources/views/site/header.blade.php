<v-app-bar :clipped-left="$vuetify.breakpoint.lgAndUp" app color="blue darken-3" dark>
    <v-spacer></v-spacer>
    <template v-if="$user.isLoggedIn()">
        <a href="/app">
            <v-btn text>
            Dashboard
            </v-btn>
        </a>
    </template>
    <template v-else>
        <a href="/login">
            <v-btn text>
                Log In
            </v-btn>
        </a>
        <a href="/register">
        <v-btn text>
            Register
        </v-btn>
        </a>
    </template>
</v-app-bar>