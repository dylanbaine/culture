<div style="max-width: 500px; margin: auto;">
    {{$slot}}
    <form action="{{route('get-started')}}" method="post">
        @csrf
        <v-text-field label="Your Name" name="name":error-messages="[
            @error('name')
                '{{$message}}'
            @enderror
        ]"></v-text-field>
        <v-text-field label="Work Email" name="email" :error-messages="[
            @error('email')
                '{{$message}}'
            @enderror
        ]"></v-text-field>
        <div class="text-center">
            <v-btn type="submit" color="primary">
                <v-icon class="mr-2">mdi-information-outline</v-icon>
                {{$buttonText ?? 'Learn More'}}
            </v-btn>
        </div>
    </form>
</div>