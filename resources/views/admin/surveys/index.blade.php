@component('admin.layout')
<h1>All Surveys</h1>
<ul>
    @foreach ($surveys as $survey)
    <li>
        <a href="{{route('surveys.show', $survey)}}">
            {{$survey->name}}
        </a>
    </li>
    @endforeach
</ul>
<a href="{{route('surveys.create')}}">
    <v-btn><v-icon>mdi-plus</v-icon> Create Survey</v-btn>
</a>
@endcomponent