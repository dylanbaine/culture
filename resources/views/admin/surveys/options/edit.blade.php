@component('admin.layout')
<div class="mb-4 d-flex">
    <a href="{{route('surveys.index')}}">Surveys</a>
    <v-icon>mdi-chevron-right</v-icon>
    <a href="{{route('surveys.show', $survey)}}">{{$survey->name}}</a>
    <v-icon>mdi-chevron-right</v-icon>
    <a href="{{route('survey-sections.show', ['surveyId' => $survey->id, 'section' => $section])}}">{{$section->name}}</a>
    <v-icon>mdi-chevron-right</v-icon>
    <b>Edit Option</b>
</div>
<v-row class="justify-center">
    <v-col cols="6">
        <v-card style="width: 600px;">
            <v-card-title>Edit Option</v-card-title>
            <v-card-text>
                <form action="{{route('survey-options.update', ['surveyId' => $survey->id, 'sectionId' => $section->id, 'option' => $option])}}" method="post">
                    @method('put')
                    @csrf
                    <v-text-field value="{{$option->option}}" label="Option" name="option"></v-text-field>
                    <v-btn type="submit">Save</v-btn>
                </form>
            </v-card-text>
        </v-card>
    </v-col>
</v-row>
@endcomponent