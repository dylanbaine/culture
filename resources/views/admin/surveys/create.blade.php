@component('admin.layout')
    <div class="mb-4 d-flex">
        <a href="{{route('surveys.index')}}">Surveys</a>
        <v-icon>mdi-chevron-right</v-icon>
        <b>Create</b>
    </div>
    <v-row class="justify-center">
        <v-col cols="6">
            <v-card style="width: 600px;">
                <v-card-title>Add Survey</v-card-title>
                <v-card-text>
                    <form action="{{route('surveys.store')}}" method="post">
                        @csrf
                        <v-text-field label="Name" name="name"></v-text-field>
                        <v-btn type="submit">Save</v-btn>
                    </form>
                </v-card-text>
            </v-card>
        </v-col>
    </v-row>
@endcomponent