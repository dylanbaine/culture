@component('admin.layout')
    <v-row class="justify-center">
        <v-col cols="12" md="8">
            <v-card style="width: 600px;">
                <v-card-title>Add Section to {{$survey->name}}</v-card-title>
                <v-card-text>
                    <form action="{{route('survey-sections.index', ['surveyId' => $survey->id])}}" method="post">
                        @csrf
                        <v-text-field label="Name" name="name"></v-text-field>
                        <v-textarea label="Description" name="description"></v-textarea>
                        <v-btn type="submit">Save</v-btn>
                    </form>
                </v-card-text>
            </v-card>
        </v-col>
    </v-row>
@endcomponent