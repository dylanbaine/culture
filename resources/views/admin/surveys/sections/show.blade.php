@component('admin.layout')
<div class="mb-4 d-flex">
    <a href="{{route('surveys.index')}}">Surveys</a>
    <v-icon>mdi-chevron-right</v-icon>
    <a href="{{route('surveys.show', $section->survey)}}">{{$section->survey->name}}</a>
    <v-icon>mdi-chevron-right</v-icon>
    <b>{{$section->name}}</b>
</div>

<div class="d-flex">
    <h1>
        {{$section->name}}
    </h1>
    <a class="ml-4" href="{{route('survey-sections.edit', ['surveyId' => $section->survey_id, 'section' => $section])}}">
        <v-btn><v-icon>mdi-pencil</v-icon>Edit</v-btn>
    </a>
</div>
@if ($section->description)
    <section>
        <h2>Description:</h2>
        <p>{{$section->description}}</p>
    </section>
@endif
<section class="mb-4">
    <h2>Options:</h2>
    <ol>
        @foreach ($section->options as $option)
            <li class="mb-8">
                {{$option->option}}
                <a href="{{route('survey-options.edit', ['surveyId' => $section->survey_id, 'sectionId' => $section->id, 'option' => $option->id])}}">
                    <v-btn text color="primary" small>
                        <v-icon class="mr-2" small>mdi-pencil</v-icon>
                        Edit
                    </v-btn>
                </a>
                <form class="d-inline" action="{{route('survey-options.destroy', ['surveyId' => $section->survey_id, 'sectionId' => $section->id, 'option' => $option->id])}}" method="post">
                    @method('delete')
                    {{ csrf_field() }}
                    <v-btn type="submit" text color="error" small>
                        <v-icon class="mr-2" small>mdi-delete</v-icon>
                        Delete
                    </v-btn>
                </form>
            </li>
        @endforeach
    </ol>
    <a class="d-block" href="{{route('survey-options.create', ['surveyId' => $section->surveyId ,'sectionId' => $section->id, 'surveyId' => $section->survey_id])}}">
        <v-btn>
            <v-icon>mdi-plus</v-icon> Add Option
        </v-btn>
    </a>
</section>
<section>
    <h2>Questions:</h2>
    <ol>
        @foreach ($section->questions as $question)
            <li class="mb-8">
                {{$question->question}}
                <a href="{{route('survey-questions.edit', ['surveyId' => $section->survey_id, 'sectionId' => $section->id, 'question' => $question->id])}}">
                    <v-btn text color="primary" small>
                        <v-icon class="mr-2" small>mdi-pencil</v-icon>
                        Edit
                    </v-btn>
                </a>
                <form class="d-inline" action="{{route('survey-questions.destroy', ['surveyId' => $section->survey_id, 'sectionId' => $section->id, 'question' => $question->id])}}" method="post">
                    @method('delete')
                    {{ csrf_field() }}
                    <v-btn type="submit" text color="error" small>
                        <v-icon class="mr-2" small>mdi-delete</v-icon>
                        Delete
                    </v-btn>
                </form>
            </li>
        @endforeach
    </ol>
    <a href="{{route('survey-questions.create', ['surveyId' => $section->surveyId ,'sectionId' => $section->id, 'surveyId' => $section->survey_id])}}">
        <v-btn>
            <v-icon>mdi-plus</v-icon> Add Question
        </v-btn>
    </a>
</section>
@endcomponent