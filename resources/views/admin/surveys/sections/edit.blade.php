@component('admin.layout')
    <v-row class="justify-center">
        <v-col cols="12" md="8">
            <v-card>
                <v-card-title>Edit Section of {{$survey->name}}</v-card-title>
                <v-card-text>
                    <form action="{{route('survey-sections.update', ['surveyId' => $survey->id, 'section' => $section])}}" method="post">
                        @csrf
                        @method('put')
                        <v-text-field label="Name" name="name" value="{{$section->name}}"></v-text-field>
                        <v-textarea label="Description" name="description" value="{{$section->description}}"></v-textarea>
                        <v-btn type="submit">Save</v-btn>
                        <a href="{{route('survey-sections.show', ['surveyId' => $section->survey_id, 'section' => $section])}}">
                            <v-btn class="ml-2">Cancel</v-btn>
                        </a>
                    </form>
                </v-card-text>
            </v-card>
        </v-col>
    </v-row>
@endcomponent