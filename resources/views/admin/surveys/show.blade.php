@component('admin.layout')
    <div class="mb-4 d-flex">
        <a href="{{route('surveys.index')}}">Surveys</a>
        <v-icon>mdi-chevron-right</v-icon>
        <b>{{$survey->name}}</b>
    </div>
    <h1>{{$survey->name}}</h1>
    <a class="mr-4" href="/take-survey/{{$survey->id}}">
        <v-btn>Preview Survey</v-btn>
    </a>
    <a href="{{route('surveys.edit', $survey)}}">
        <v-btn>Edit Name</v-btn>
    </a>
    <form class="d-inline ml-4" action="{{route('surveys.destroy', $survey)}}" method="POST">
        @csrf
        @method('delete')
        <v-btn type="submit" color="error">
            <v-icon>mdi-delete</v-icon>
            Delete
        </v-btn>
    </form>
    <h2>Sections:</h2>
    <ul style="list-style: none;">
        @foreach ($survey->sections as $section)
            <li class="mb-2">
                <a href="{{route('survey-sections.show', ['section' => $section->id, 'surveyId' => $survey->id])}}">
                    {{$section->name}} - ({{$section->questions()->count()}} questions)
                </a>
            </li>
            @endforeach
    </ul>
    <a href="{{route('survey-sections.create', ['surveyId' => $survey->id])}}">
        <v-btn>
            <v-icon>mdi-plus</v-icon> Add Section
        </v-btn>
    </a>
@endcomponent