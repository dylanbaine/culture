@component('admin.layout')
<div class="mb-4 d-flex">
    <a href="{{route('surveys.index')}}">Surveys</a>
    <v-icon>mdi-chevron-right</v-icon>
    <a href="{{route('surveys.show', $survey)}}">{{$survey->name}}</a>
    <v-icon>mdi-chevron-right</v-icon>
    <a href="{{route('survey-sections.show', ['surveyId' => $survey->id, 'section' => $section])}}">{{$section->name}}</a>
    <v-icon>mdi-chevron-right</v-icon>
    <b>Edit Question</b>
</div>
<v-row class="justify-center">
    <v-col cols="6">
        <v-card style="width: 600px;">
            <v-card-title>Edit Question</v-card-title>
            <v-card-text>
                <form action="{{route('survey-questions.update', ['surveyId' => $survey->id, 'sectionId' => $section->id, 'question' => $question])}}" method="post">
                    @method('put')
                    @csrf
                    <v-text-field value="{{$question->question}}" label="Question" name="question"></v-text-field>
                    <ol class="mb-2">
                        @foreach ($section->options as $option)
                            @php
                                $value = $option->valueForQuestion($question)
                            @endphp
                            <li>
                                {{$option->option}}
                                <br>
                                <select name="options[{{$option->id}}][operator]" class="bare">
                                    <option value=""></option>
                                    <option {{optional($value)->operator == '-' ? 'selected' : ''}} value="-">-</option>
                                    <option {{optional($value)->operator == '+' ? 'selected' : ''}} value="+">+</option>
                                </select>
                                <input placeholder="Value" value="{{optional($value)->value}}" type="text" name="options[{{$option->id}}][value]" class="bare">
                            </li>
                        @endforeach
                    </ol>
                    <p>
                        <input type="hidden" value="off" name="configurations[useCheckBox]">
                        <label>
                            Use Custom Textbox
                            <input type="checkbox" {{ $question->useCheckbox ? '' : 'checked'}} name="configurations[useTextarea]">
                        </label>
                    </p>
                    <v-btn type="submit">Save</v-btn>
                </form>
            </v-card-text>
        </v-card>
    </v-col>
</v-row>
@endcomponent