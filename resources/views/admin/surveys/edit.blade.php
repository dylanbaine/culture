@component('admin.layout')
    <v-row class="justify-center">
        <v-col cols="6">
            <v-card style="width: 600px;">
                <v-card-title>Edit Survey</v-card-title>
                <form action="{{route('surveys.update', $survey)}}" method="post">
                    <v-card-text>
                            @csrf
                            @method('put')
                            <v-text-field label="Name" name="name" value="{{$survey->name}}"></v-text-field>
                        </v-card-text>
                        <v-card-actions>
                            <v-btn type="submit">Save</v-btn>
                            <a class="ml-4" href="{{route('surveys.show', $survey)}}">
                                <v-btn>Cancel</v-btn>
                            </a>
                    </v-card-actions>
                </form>
            </v-card>
        </v-col>
    </v-row>
@endcomponent