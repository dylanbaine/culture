@extends('layouts.main')

@push('scripts')
    <script src="{{asset('/js/admin.js')}}"></script>
@endpush

@section('content')
<v-navigation-drawer :value="true" :clipped="$vuetify.breakpoint.lgAndUp" app>
    <v-list dense>
        <a href="/app">
            <v-list-item>
                App
            </v-list-item>
        </a>
        <a href="{{route('surveys.index')}}">
            <v-list-item>
                Surveys
            </v-list-item>
        </a>
    </v-list>
</v-navigation-drawer>
<v-main>
    <v-container fluid>
        {{$slot}}
    </v-container>
</v-main>
@endsection
