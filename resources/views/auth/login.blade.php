@extends('layouts.main')

@push('scripts')
    <script src="{{ asset('js/site.js') }}" defer></script>
@endpush

@section('content')
@include('site.header')
<v-main>
    <v-container fluid>
        <div class="d-flex justify-center">
            <v-card class="mt-10" style="width: 600px">
                <v-card-text>
                    <h1>Log In</h1>
                    Don't have an account? <a href="{{route('register')}}">Register</a>
                </v-card-text>
                <v-form method="POST" action="{{ route('login') }}">
                    <v-card-text>
                        @csrf
                        <v-text-field label="Email" type="email" @error('email') is-invalid @enderror
                            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus :error-messages="[
                                @error('email')
                                        '{{ $message }}'
                                @enderror
                            ]"></v-text-field>
                        <v-text-field label="Password" id="password" type="password" @error('password') is-invalid @enderror
                            name="password" required autocomplete="current-password" :error-messages="[
                                @error('password')
                                        '{{ $message }}'
                                @enderror
                            ]"></v-text-field>
                        <div>
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </v-card-text>
                    <v-card-actions>
                        <v-btn type="submit" color="primary">
                            {{ __('Login') }}
                        </v-btn>

                        @if (Route::has('password.request'))
                            <a style="text-decoration: none;" href="{{ route('password.request') }}">
                                <v-btn>
                                    {{ __('Forgot Your Password?') }}
                                </v-btn>
                            </a>
                        @endif
                    </v-card-actions>
                </v-form>
            </v-card>
        </div>
    </v-container>
</v-main>
@endsection
