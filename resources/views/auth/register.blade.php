@php
    $organization = App\Models\Organization::find(request('organization'));
@endphp

@extends('layouts.main')

@push('scripts')
    <script src="{{ asset('js/site.js') }}" defer></script>
@endpush

@section('content')
@include('site.header')
<v-main>
    <v-container fluid>
        <div class="d-flex justify-center">
            <v-card class="mt-10" style="width: 600px">
                <v-card-text>
                    <h1>
                        @if ($organization)
                            Join {{$organization->name}} on {{config('app.name')}}
                        @else
                            Create your account
                        @endif
                    </h1>
                    Already have an account? <a href="{{route('login')}}">Log in</a>
                    <v-form action="{{action("Auth\RegisterController@register")}}" method="post">
                        @csrf
                        @error('organization_id')
                            <v-alert border="left" class="mt-4 white--text" color="error">
                                {{ $message }}
                            </v-alert>
                        @enderror
                        @if (!$organization)
                            <v-text-field name="organization_name" label="Your Organization" :error-messages="[
                                @error('organization_name')
                                    '{{ $message }}'
                                @enderror
                            ]"></v-text-field>
                        @else
                            <input type="hidden" name="organization_id" value="{{$organization->id}}">
                        @endif
                        <v-text-field name="name" value="{{request('name')}}" label="Your Name" :error-messages="[
                            @error('name')
                                '{{ $message }}'
                            @enderror
                        ]"></v-text-field>
                        <v-text-field name="email" value="{{request('email')}}" label="Your Email" type="email" :error-messages="[
                            @error('email')
                                '{{ $message }}'
                            @enderror
                        ]"></v-text-field>
                        <v-text-field name="password" label="Password" type="password"></v-text-field>
                        <v-btn type="submit" color="primary">Create Account</v-btn>
                    </v-form>
                </v-card-text>
            </v-card>
        </div>
    </v-container>
</v-main>
@endsection
