@extends('layouts.main')

@push('scripts')
    <script src="{{ asset('js/site.js') }}" defer></script>
@endpush

@section('content')
@include('site.header')
<v-main>
    <v-container fluid>
        <div class="d-flex justify-center">
            <v-card class="mt-10" style="width: 600px">
                <v-card-text>
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">

                        <v-text-field label="Your Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus :error-messages="[
                                @error('email')
                                    '{{ $message }}'
                                @enderror
                            ]"></v-text-field>

                        <v-text-field label="New Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                            name="password" required autocomplete="new-password" :error-messages="[
                                @error('password')
                                    '{{ $message }}'
                                @enderror
                            ]"></v-text-field>

                        <v-text-field label="Confirm Password" id="password-confirm" type="password" class="form-control"
                            name="password_confirmation" required autocomplete="new-password"></v-text-field>

                        <v-btn type="submit">
                            Reset Password
                        </v-btn>
                    </form>
                </v-card-text>
            </v-card>
        </div>
    </v-container>
</v-main>
@endsection
