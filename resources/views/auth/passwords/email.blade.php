@extends('layouts.main')

@push('scripts')
    <script src="{{ asset('js/site.js') }}" defer></script>
@endpush

@section('content')
@include('site.header')
<v-main>
    <v-container fluid>
        <div class="d-flex justify-center">
            <v-card class="mt-10" style="width: 600px">
                <v-card-text>
                    <h1>Reset your password.</h1>
                    <a class="error--text" href="{{route('login')}}"> <v-icon color="error" x-small>mdi-chevron-left</v-icon> Cancel</a>
                    @if (session('status'))
                        <v-alert border="left" class="mt-4 white--text" color="success">
                            {{ session('status') }}
                        </v-alert>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <v-text-field label="Your Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus :error-messages="[
                                @error('email')
                                    '{{ $message }}'
                                @enderror
                            ]"></v-text-field>
                        <v-btn type="submit">
                            Send Password Reset Link
                        </v-btn>
                    </form>
                </v-card-text>
            </v-card>
        </div>
    </v-container>
</v-main>
@endsection
