export default [
    { path: '', component: () => import('./pages/index.vue') },
    { path: '/confirm', component: () => import('./pages/confirm.vue') },
    {
        name: 'show-section',
        path: '/section/:section',
        component: () => import('./pages/section.vue'),
        children: [
            {
                name: 'show-question',
                path: 'question/:question',
                component: () => import('./pages/question.vue'),
            }
        ]
    },
]