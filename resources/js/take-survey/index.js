import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'

const { vuetify } = require('../bootstrap')

Vue.use(VueRouter)
const router = new VueRouter({
    routes,
})

new Vue({
    el: '#app',
    data: {
        survey: window.survey || {},
    },
    vuetify,
    router,
})
