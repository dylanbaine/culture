export function notify(msg, type = 'info') {
    if (!this.$root.notify) {
        console.warn('This Vue instance doesn\'t have notify propery. You need to add one in order to use the this.$notify() method.')
        return
    }
    this.$root.notify.showing = true
    this.$root.notify.message = msg
    this.$root.notify.type = type
}