
export default [
    { path: '/', component: () => import('./pages/index.vue') },
    { path: '/profile', component: () => import('./pages/profile.vue') },
    { path: '/notifications', component: () => import('./pages/notifications.vue') },
    {
        path: '/surveys',
        component: () => import('./pages/surveys/index.vue'),
        children: [
            { path: 'send', component: () => import('./pages/surveys/send.vue') },
        ],
    },
    {
        path: '/surveys/reports',
        component: () => import('./pages/surveys/reports.vue'),
    },
    {
        path: '/surveys/schedule',
        component: () => import('./pages/surveys/schedule.vue'),
    },
    { path: '/employees', component: () => import('./pages/employees/index.vue'), children: [
        { path: 'upload', component: () => import('./pages/employees/upload.vue') },
        { path: 'invite', component: () => import('./pages/employees/invites/create.vue') },
    ] },
    { path: '/employees/invites', component: () => import('./pages/employees/invites/index.vue') },
    { path: '/employees/:user', component: () => import('./pages/employees/show.vue'), children: [
        { path: 'delete', component: () => import('./pages/employees/delete.vue') },
    ] },
    { path: '/settings', component: () => import('./pages/settings.vue') },
    { path: '/help-center', component: () => import('./pages/help-center.vue') },
    { path: '/feedback', component: () => import('./pages/feedback.vue') },
]
