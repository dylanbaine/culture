export function query (axios) {
    return function(query, variables = {}, file = false) {
        return axios.post('/graphql', { query, variables })
            .then(({ data }) => {
                if (data.errors) {
                    this.$notify(data.errors.map((e) => e.debugMessage || e.message).join(' '), 'error')
                    throw new Error(data.errors[0].message)
                }
                return data.data
            })
    }
}