import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'

const { vuetify } = require('../bootstrap')

Vue.use(VueRouter)
const router = new VueRouter({
    routes,
})

new Vue({
    el: '#app',
    data: {
        drawer: true,
    },
    vuetify,
    router,
    mounted() {
        if (window.state.error) {
            this.$notify(window.state.error, 'error')
        }
        if (window.state.success) {
            this.$notify(window.state.success, 'success')
        } 
    
    }
})
