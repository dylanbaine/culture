export class User {
    constructor(user) {
        this.user = user
    }

    get id () {
        return this.user.id
    }

    get name () {
        return this.user.name
    }

    get email () {
        return this.user.email
    }

    get organization () {
        if (!this.isLoggedIn()) {
            return null
        }
        return this.user.organization
    }

    get role () {
        if (!this.isLoggedIn()) {
            return null
        }
        return this.user.role
    }

    isLoggedIn() {
        return this.user !== null
    }

    canManageEmployees() {
        return this.isAdmin()
    }

    canTriggerSurveys() {
        return this.isAdmin()
    }

    canViewReports() {
        return this.isAdmin()
    }

    canScheduleSurveys() {
        return this.isAdmin()
    }

    isAdmin() {
        return this.user.role.name == 'admin'
    }
}