
import axios from 'axios'
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import { User } from './app/User'
import { query } from './app/query'
import { notify } from './app/notify'

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

Vue.use(Vuetify)
const vuetifyOptions = {}
export const vuetify = new Vuetify(vuetifyOptions)

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

const { user } = window.state

Vue.mixin({
    data() {
        return {
            notify: {
                showing: false,
                message: '',
                type: null
            },
        }
    }
})

Vue.prototype.$csrf = window.csrf
Vue.prototype.$user = new User(user)
Vue.prototype.$query = query(axios)
Vue.prototype.$notify = notify