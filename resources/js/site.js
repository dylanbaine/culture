import Vue from 'vue'

const { vuetify } = require('./bootstrap')

new Vue({
    el: '#app',
    data: {
        drawer: true,
        notify: {
            showing: false,
            message: '',
            type: null
        }
    },
    vuetify,
    mounted() {
        if (window.state.error) {
            this.$notify(window.state.error, 'error')
        }
        if (window.state.success) {
            this.$notify(window.state.success, 'success')
        } 
    
    }
})
