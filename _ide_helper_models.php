<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Survey
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SurveySection[] $sections
 * @property-read int|null $sections_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey whereUpdatedAt($value)
 */
	class Survey extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OptionScoreValue
 *
 * @property int $id
 * @property int $question_id
 * @property int $option_id
 * @property int $value
 * @property string|null $operator
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\SurveyQuestionOption $option
 * @property-read \App\Models\SurveyQuestion $question
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OptionScoreValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OptionScoreValue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OptionScoreValue query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OptionScoreValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OptionScoreValue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OptionScoreValue whereOperator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OptionScoreValue whereOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OptionScoreValue whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OptionScoreValue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OptionScoreValue whereValue($value)
 */
	class OptionScoreValue extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SurveyQuestionOption
 *
 * @property int $id
 * @property int $section_id
 * @property string $option
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SurveyQuestionAnswer[] $answers
 * @property-read int|null $answers_count
 * @property-read \App\Models\SurveySection $section
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionOption newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionOption newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionOption query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionOption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionOption whereOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionOption whereSectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionOption whereUpdatedAt($value)
 */
	class SurveyQuestionOption extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserSurvey
 *
 * @property int $id
 * @property int $user_id
 * @property int $survey_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SurveyQuestionAnswer[] $answers
 * @property-read int|null $answers_count
 * @property-read \App\Models\Survey $survey
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSurvey newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSurvey newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSurvey query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSurvey whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSurvey whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSurvey whereSurveyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSurvey whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSurvey whereUserId($value)
 */
	class UserSurvey extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property int|null $role_id
 * @property int|null $organization_id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Organization|null $organization
 * @property-read \App\Models\UserRole|null $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereOrganizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SurveySection
 *
 * @property int $id
 * @property int $survey_id
 * @property string $name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SurveyQuestionOption[] $options
 * @property-read int|null $options_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SurveyQuestion[] $questions
 * @property-read int|null $questions_count
 * @property-read \App\Models\Survey $survey
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveySection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveySection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveySection query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveySection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveySection whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveySection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveySection whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveySection whereSurveyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveySection whereUpdatedAt($value)
 */
	class SurveySection extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AdminUser
 *
 * @property int $id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUser whereUserId($value)
 */
	class AdminUser extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SurveyQuestionConfiguration
 *
 * @property int $id
 * @property int $question_id
 * @property string $key
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionConfiguration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionConfiguration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionConfiguration query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionConfiguration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionConfiguration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionConfiguration whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionConfiguration whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionConfiguration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionConfiguration whereValue($value)
 */
	class SurveyQuestionConfiguration extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SurveyQuestionAnswer
 *
 * @property int $id
 * @property int $question_id
 * @property int $option_id
 * @property int $user_survey_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\SurveyQuestionOption $option
 * @property-read \App\Models\SurveyQuestion $question
 * @property-read \App\Models\UserSurvey $userSurvey
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionAnswer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionAnswer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionAnswer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionAnswer whereOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionAnswer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestionAnswer whereUserSurveyId($value)
 */
	class SurveyQuestionAnswer extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OrganizationInvite
 *
 * @property int $id
 * @property int $organization_id
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Organization $organization
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationInvite newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationInvite newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationInvite query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationInvite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationInvite whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationInvite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationInvite whereOrganizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationInvite whereUpdatedAt($value)
 */
	class OrganizationInvite extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserRole
 *
 * @property int $id
 * @property int $organization_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRole query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRole whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRole whereOrganizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRole whereUpdatedAt($value)
 */
	class UserRole extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Organization
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrganizationInvite[] $invites
 * @property-read int|null $invites_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserRole[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereUpdatedAt($value)
 */
	class Organization extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SurveyQuestion
 *
 * @property int $id
 * @property int $section_id
 * @property string $question
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SurveyQuestionConfiguration[] $configurations
 * @property-read int|null $configurations_count
 * @property-read mixed $use_checkbox
 * @property-read \App\Models\SurveySection $section
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestion query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestion whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestion whereSectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SurveyQuestion whereUpdatedAt($value)
 */
	class SurveyQuestion extends \Eloquent {}
}

