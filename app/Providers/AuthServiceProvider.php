<?php

namespace App\Providers;

use App\Models\OrganizationInvite;
use App\Models\Survey;
use App\Models\User;
use App\Policies\OrganizationInvitePolicy;
use App\Policies\SurveyPolicy;
use App\Policies\UsersPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UsersPolicy::class,
        OrganizationInvite::class => OrganizationInvitePolicy::class,
        Survey::class => SurveyPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
