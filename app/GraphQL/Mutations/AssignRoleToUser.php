<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use App\Models\UserRole;
use Error;

class AssignRoleToUser
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args): User
    {
        $user = User::findOrFail($args['user_id']);
        $role = UserRole::findOrFail($args['role_id']);
        if (!$user->sharesOrganization($role)) {
            throw new Error('Role not permitted for user');
        }
        $user->role_id = $role->id;
        $user->save();
        return $user->refresh();
    }
}
