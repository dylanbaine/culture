<?php

namespace App\GraphQL\Mutations;

use App\Jobs\SendSurveyToOrganizationUsers;
use App\Models\Survey;

class SendSurveyToOrganization
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        $survey = Survey::find($args['surveyId']);
        dispatch(new SendSurveyToOrganizationUsers(user(), $survey));
        return $survey;
    }
}
