<?php

namespace App\GraphQL\Mutations;

use App\Mail\InviteUserToOrganization;
use App\Models\OrganizationInvite;
use Error;
use Mail;

class CreateOrganizationInvite
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args): OrganizationInvite
    {
        $payload = [
            'email' => $args['email'],
            'organization_id' => organization()->id,
        ];
        if (OrganizationInvite::where($payload)->exists()) {
            throw new Error("User already invited");
        }
        $invite = OrganizationInvite::create($payload);
        Mail::to($args['email'])->send(new InviteUserToOrganization(user(), $invite->email));
        return $invite;
    }
}
