<?php

namespace App\GraphQL\Mutations;

class RemoveSurveySchedule
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        return user()->organization->surveySchedule()->whereSurveyId(
            $args['surveyId']
        )->whereFrequencyId(
            $args['frequencyId']
        )->delete();
    }
}
