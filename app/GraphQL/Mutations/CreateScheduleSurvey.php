<?php

namespace App\GraphQL\Mutations;

use App\Models\SurveySchedule;

class CreateScheduleSurvey
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        $input = $args['input'];
        return SurveySchedule::create([
            'survey_id' => $input['surveyId'],
            'frequency_id' => $input['frequencyId'],
            'organization_id' => user()->organization_id,
        ]);
    }
}
