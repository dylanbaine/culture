<?php

namespace App\GraphQL\Mutations;

use App\Models\Survey;
use App\Models\UserSurvey;

class MarkSurveyAsFinished
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        $survey = Survey::findOrFail($args['surveyId']);
        UserSurvey::whereUserId(user()->id)
            ->whereSurveyId($survey->id)
            ->first()
            ->update([
                'is_complete' => true,
            ]);
        return $survey;
    }
}
