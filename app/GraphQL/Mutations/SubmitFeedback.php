<?php

namespace App\GraphQL\Mutations;

use App\Jobs\NotifySiteAdmins;
use App\Models\AdminUser;
use App\Models\UserFeedback;
use Illuminate\Mail\Message;
use Mail;

class SubmitFeedback
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        $user = user();
        dispatch(new NotifySiteAdmins(
            "User: {$user->name}\n" .
            "Organization: {$user->organization->name}\n" .
            "User Feedback:\n{$args['content']}",
            'New user feedback for '.config('app.name'))
        );
        return UserFeedback::create([
            'user_id' => $user->id,
            'content' => $args['content'],
        ]);
    }
}
