<?php

namespace App\GraphQL\Mutations;

use App\Exceptions\AnswerSurveyException;
use App\Models\SurveyQuestion;
use App\Models\SurveyQuestionAnswer;
use App\Models\SurveyQuestionTextAnswer;
use App\Models\UserSurvey;
use Illuminate\Database\Eloquent\Model;

class AnswerSurvey
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args): ?Model
    {
        $input = $args['input'];
        $userSurvey = UserSurvey::whereSurveyId($input['surveyId'])
            ->whereUserId(user()->id)
            ->first();
        if (user()->isSiteAdmin() && !$userSurvey) {
            return null;
        }
        $question = SurveyQuestion::find($input['questionId']);
        if (!$question) {
            throw new AnswerSurveyException('question not found');
        }
        if (!$userSurvey) {
            throw new AnswerSurveyException('user survey not found');
        }
        if ($question->use_checkbox) {
            return $this->storeSelectedAnswer($input, $userSurvey);
        }
        return $this->storeTextAnswer($input, $userSurvey);
    }

    protected function storeSelectedAnswer(array $input, UserSurvey $userSurvey)
    {
        $currentAnswerToQuestion = SurveyQuestionAnswer::where([
            'question_id' => $input['questionId'],
            'user_survey_id' => $userSurvey->id
        ])->first();
        if ($currentAnswerToQuestion) {
            $currentAnswerToQuestion->option_id = $input['optionId'];
            $currentAnswerToQuestion->save();
            return $currentAnswerToQuestion;
        }
        return SurveyQuestionAnswer::create([
            'question_id' => $input['questionId'],
            'user_survey_id' => $userSurvey->id,
            'option_id' => $input['optionId'],
        ]);
    }

    protected function storeTextAnswer(array $input, UserSurvey $userSurvey)
    {
        $currentAnswerToQuestion = SurveyQuestionTextAnswer::where([
            'question_id' => $input['questionId'],
            'user_survey_id' => $userSurvey->id,
        ])->first();
        if ($currentAnswerToQuestion) {
            $currentAnswerToQuestion->answer = $input['manualInput'];
            $currentAnswerToQuestion->save();
            return $currentAnswerToQuestion;
        }
        return SurveyQuestionTextAnswer::create([
            'question_id' => $input['questionId'],
            'user_survey_id' => $userSurvey->id,
            'answer' => $input['manualInput'],
        ]);
    }
}
