<?php

namespace App\GraphQL\Mutations;

use Illuminate\Support\Facades\Mail;
use App\Mail\InviteUserToOrganization;
use App\Models\OrganizationInvite;
use Error;

class ResendOrganizationInvite
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        $invite = OrganizationInvite::find($args['id']);
        if (!$invite) {
            throw new Error("invite not found");
        }
        Mail::to($invite->email)->send(new InviteUserToOrganization(user(), $invite->email));
        return $invite;
    }
}
