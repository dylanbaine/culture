<?php

namespace App\GraphQL\Queries;

use App\Models\Survey;

class AvailableSurveys
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        return Survey::with('sections')->get();
    }
}
