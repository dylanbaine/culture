<?php

namespace App\GraphQL\Queries;

use App\Models\Survey;

class PendingSurveys
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        $surveyIds = user()->surveys()->whereIsComplete(false)->get(['survey_id']);
        return Survey::whereIn('id', $surveyIds)->get();
    }
}
