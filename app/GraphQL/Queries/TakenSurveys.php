<?php

namespace App\GraphQL\Queries;

class TakenSurveys
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        return user()->surveys()->with('survey')->whereIsComplete(1)->paginate($args['limit'] ?? 12);
    }
}
