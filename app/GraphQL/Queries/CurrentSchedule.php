<?php

namespace App\GraphQL\Queries;

class CurrentSchedule
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        return user()->organization->surveySchedule;
    }
}
