<?php

namespace App\GraphQL\Queries;

class TakenSurveysCount
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        return user()->surveys()->whereIsComplete(1)->count();
    }
}
