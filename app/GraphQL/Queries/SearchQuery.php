<?php

namespace App\GraphQL\Queries;

class SearchQuery
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args): array
    {
        $term = $args['term'] ?? false;
        $usersQuery = organization()
            ->users()
            ->with('role');
        if (!$term) {
            return $usersQuery
                ->take(4)
                ->get()
                ->map([$this, 'formatUser'])
                ->toArray();
        }
        $users = $usersQuery
            ->where(function($users) use ($term) {
                return $users->where('name', 'like', "%{$term}%")
                    ->orWhere('email', 'like', "%{$term}%");
            })
            ->get()
            ->map([$this, 'formatUser'])
            ->toArray();
        return $users;
    }

    public function formatUser($user)
    {
        return [
            'text' => "{$user->name} ({$user->role->name})",
            'value' => "/employees/{$user->id}"
        ];
    }
}
