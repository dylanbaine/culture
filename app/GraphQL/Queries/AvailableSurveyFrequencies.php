<?php

namespace App\GraphQL\Queries;

use App\Models\SurveyScheduleFrequency;

class AvailableSurveyFrequencies
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        return SurveyScheduleFrequency::get();
    }
}
