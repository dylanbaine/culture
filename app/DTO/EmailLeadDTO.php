<?php

namespace App\DTO;

class EmailLeadDTO
{
    public $source,
        $email,
        $name;
}
