<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionConfiguration extends Model
{
    protected $fillable = ['question_id', 'key', 'value'];
}
