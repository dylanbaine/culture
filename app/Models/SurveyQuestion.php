<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestion extends Model
{
    protected $fillable = ['question', 'section_id'];

    protected $appends = ['use_checkbox'];

    public function section()
    {
        return $this->belongsTo(SurveySection::class, 'section_id');
    }

    public function configurations()
    {
        return $this->hasMany(SurveyQuestionConfiguration::class, 'question_id');
    }

    public function getUseCheckboxAttribute()
    {
        return optional($this->configurations()->where('key', 'useTextarea')->first())
            ->value == 0;
    }

}
