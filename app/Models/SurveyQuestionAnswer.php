<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionAnswer extends Model
{
    protected $fillable = ['question_id', 'option_id', 'user_survey_id'];

    public function question()
    {
        return $this->belongsTo(SurveyQuestion::class);
    }

    public function option()
    {
        return $this->belongsTo(SurveyQuestionOption::class);
    }

    public function userSurvey()
    {
        return $this->belongsTo(UserSurvey::class);
    }
}
