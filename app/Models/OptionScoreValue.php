<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OptionScoreValue extends Model
{

    protected $fillable = ['option_id', 'question_id', 'value', 'operator'];

    protected $casts = [
        'value' => 'integer',
    ];

    public function option()
    {
        return $this->belongsTo(SurveyQuestionOption::class);
    }

    public function question()
    {
        return $this->belongsTo(SurveyQuestion::class);
    }
}
