<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{

    protected $fillable = ['name'];
    
    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function roles()
    {
        return $this->hasMany(UserRole::class);
    }

    public function invites()
    {
        return $this->hasMany(OrganizationInvite::class);
    }

    public function createDefaultRoles()
    {
        $defaultRoles = ['admin', 'employee'];
        foreach($defaultRoles as $role) {
            $this->roles()->create(['name' => $role]);
        }
        return $this;
    }

    public function surveySchedule()
    {
        return $this->hasMany(SurveySchedule::class);
    }

}
