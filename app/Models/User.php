<?php

namespace App\Models;

use Error;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'role_id', 'email', 'password', 'organization_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isSiteAdmin()
    {
        return AdminUser::whereUserId($this->id)->exists();
    }

    public function isAdmin(): bool
    {
        return $this->role->name === 'admin';
    }

    public function sharesOrganization(Model $belongsToOrganization)
    {
        if (!($belongsToOrganization->organization_id ?? false)) {
            throw new Error("model does not have an organization_id");
        }
        return $this->organization_id == $belongsToOrganization->organization_id;
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function role()
    {
        return $this->belongsTo(UserRole::class);
    }

    public function surveys()
    {
        return $this->hasMany(UserSurvey::class);
    }

    public function feedback()
    {
        return $this->hasMany(UserFeedback::class);
    }
}
