<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionTextAnswer extends Model
{
    protected $fillable = ['question_id', 'user_survey_id', 'answer'];
}
