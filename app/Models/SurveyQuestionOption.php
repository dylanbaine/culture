<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionOption extends Model
{
    protected $fillable = ['option', 'section_id'];

    public function section()
    {
        return $this->belongsTo(SurveySection::class);
    }

    public function answers()
    {
        return $this->hasMany(SurveyQuestionAnswer::class);
    }

    public function valueForQuestion(SurveyQuestion $question): ?OptionScoreValue
    {
        return OptionScoreValue::whereQuestionId($question->id)
            ->whereOptionId($this->id)
            ->first();
    }
}
