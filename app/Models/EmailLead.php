<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailLead extends Model
{
    protected $fillable = ['source', 'email', 'name'];
}
