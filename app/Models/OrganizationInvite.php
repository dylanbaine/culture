<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganizationInvite extends Model
{
    protected $fillable = [
        'organization_id', 'email'
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
