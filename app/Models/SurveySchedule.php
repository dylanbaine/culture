<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveySchedule extends Model
{
    protected $fillable = [
        'organization_id', 'survey_id', 'frequency_id',
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function frequency()
    {
        return $this->belongsTo(SurveyScheduleFrequency::class);
    }
}
