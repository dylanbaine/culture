<?php
namespace App\Policies\Concerns;

trait EasyBefore
{
    public function before()
    {
        if (config('app.env') == 'local') {
            return true;
        }
    }
}