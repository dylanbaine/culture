<?php

namespace App\Policies;

use App\Models\User;
use App\Models\OrganizationInvite;
use App\Policies\Concerns\EasyBefore;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrganizationInvitePolicy
{
    use HandlesAuthorization, EasyBefore;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrganizationInvite  $organizationInvite
     * @return mixed
     */
    public function view(User $user, OrganizationInvite $organizationInvite)
    {
        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrganizationInvite  $organizationInvite
     * @return mixed
     */
    public function update(User $user, OrganizationInvite $organizationInvite)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrganizationInvite  $organizationInvite
     * @return mixed
     */
    public function delete(User $user, OrganizationInvite $organizationInvite)
    {
        return $user->isAdmin() && $user->sharesOrganization($organizationInvite);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrganizationInvite  $organizationInvite
     * @return mixed
     */
    public function restore(User $user, OrganizationInvite $organizationInvite)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrganizationInvite  $organizationInvite
     * @return mixed
     */
    public function forceDelete(User $user, OrganizationInvite $organizationInvite)
    {
        return false;
    }
}
