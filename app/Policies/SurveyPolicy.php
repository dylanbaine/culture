<?php

namespace App\Policies;

use App\Models\Survey;
use App\Models\User;
use App\Models\UserSurvey;
use Illuminate\Auth\Access\HandlesAuthorization;

class SurveyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    public function answer(User $user, Survey $survey)
    {
        return $user->isSiteAdmin() || $this->view($user, $survey);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Survey  $survey
     * @return mixed
     */
    public function view(User $user, Survey $survey)
    {
        return UserSurvey::whereUserId($user->id, $survey->id)->exists() || user()->isSiteAdmin();
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return user()->isSiteAdmin();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Survey  $survey
     * @return mixed
     */
    public function update(User $user, Survey $survey)
    {
        return user()->isSiteAdmin();
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Survey  $survey
     * @return mixed
     */
    public function delete(User $user, Survey $survey)
    {
        return user()->isSiteAdmin();
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Survey  $survey
     * @return mixed
     */
    public function restore(User $user, Survey $survey)
    {
        return user()->isSiteAdmin();
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Survey  $survey
     * @return mixed
     */
    public function forceDelete(User $user, Survey $survey)
    {
        return user()->isSiteAdmin();
    }
}
