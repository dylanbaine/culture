<?php

namespace App\Actions;

use App\DTO\EmailLeadDTO;
use App\Models\EmailLead;

class StoreEmailLeadAction
{

    public function execute(EmailLeadDTO $data): EmailLead
    {
        if (
            $existing = EmailLead::whereSource($data->source)->whereEmail($data->email)->first()
        ) {
            $existing->name = $data->name;
            $existing->save();
            return $existing;
        }
        return EmailLead::create((array)$data);
    }
}
