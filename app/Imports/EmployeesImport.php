<?php

namespace App\Imports;

use App\Jobs\SendInviteLinksToEmailBatch;
use App\Mail\InviteUserToOrganization;
use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EmployeesImport implements WithHeadingRow
{}
