<?php

namespace App\Jobs;

use App\Mail\InviteUserToOrganization;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class BatchImportEmployees implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $csv, $user;
    public function __construct($csv, $user)
    {
        $this->user = $user;
        $this->csv = $csv;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach($this->csv as $csv) {
            foreach($csv as $row) {
                $email = $row['email'];
                $this->user->organization->invites()->create([
                    'email' => $email
                ]);
                Mail::to($email)
                    ->send(new InviteUserToOrganization($this->user, $email));
                sleep(3);
            }
        }
    }
}
