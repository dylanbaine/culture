<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Models\SurveyQuestion;
use App\Models\OptionScoreValue;
use App\Models\SurveyQuestionConfiguration;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreSurveyQuestion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $surveyQuestion;

    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data, ?SurveyQuestion $surveyQuestion = null)
    {
        $this->surveyQuestion = $surveyQuestion;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->surveyQuestion) {
            $this->surveyQuestion->update([
                'question' => $this->data['question'],
            ]);
        } else {
            $this->surveyQuestion = SurveyQuestion::create([
                'question' => $this->data['question'],
                'section_id' => $this->data['sectionId']
            ]);
        }
        $this->saveConfigurations();
        $this->saveOptionValues();
    }

    public function getResult()
    {
        return $this->surveyQuestion->reload();
    }

    public function saveConfigurations()
    {
        foreach($this->data['configurations'] ?? [] as $key => $value) {
            $config = SurveyQuestionConfiguration::firstOrNew([
                'question_id' => $this->surveyQuestion->id,
                'key' => $key,
            ]);
            if (in_array($value, ['on', 'off'])) {
                $config->value = $value == 'on' ? '1' : '0';
            } else {
                $config->value = $value;
            }
            $config->save();
        }
    }

    public function saveOptionValues()
    {
        foreach($this->data['options'] ?? [] as $optionId => $value) {
            if (is_null($value['value'])) {
                continue;
            }
            $answerScore = OptionScoreValue::firstOrNew([
                'option_id' => $optionId,
                'question_id' => $this->surveyQuestion->id,
            ]);
            $answerScore->value = $value['value'];
            $answerScore->operator = $value['operator'];
            $answerScore->save();
        }
    }
}
