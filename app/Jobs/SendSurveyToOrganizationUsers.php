<?php

namespace App\Jobs;

use App\Models\Survey;
use App\Models\User;
use App\Models\UserSurvey;
use App\Notifications\TakeSurveyNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Notification;

class SendSurveyToOrganizationUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user, $survey;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Survey $survey)
    {
        $this->user = $user;
        $this->survey = $survey;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $orgUsers = $this->user->organization->users;
        UserSurvey::insert(
            $orgUsers->map(function(User $user) {
                return [
                    'user_id' => $user->id,
                    'survey_id' => $this->survey->id,
                    'is_complete' => 0,
                    'created_at' => now(),
                ];
            })->all()
        );
        Notification::send($orgUsers, new TakeSurveyNotification($this->survey));
    }
}
