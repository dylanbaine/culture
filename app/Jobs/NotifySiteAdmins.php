<?php

namespace App\Jobs;

use App\Models\AdminUser;
use Illuminate\Mail\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifySiteAdmins implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $content, $subject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $content, string $subject)
    {
        $this->content = $content;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::raw($this->content, function(Message $mail) {
            $mail->to(AdminUser::whereHas('user')->with('user')->get()->pluck('user.email')->toArray())
                ->subject($this->subject);
        });
    }
}
