<?php

namespace App\Mail;

use App\Models\OrganizationInvite;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InviteUserToOrganization extends Mailable
{
    use Queueable, SerializesModels;


    public $invitedBy, $email, $name;

    public function __construct(?User $invitedBy = null, string $email, ?string $name = null)
    {
        $this->invitedBy = $invitedBy;
        $this->email = $email;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $appName = config('app.name');
        return $this->markdown('invite-user-to-organization')
            ->subject(
                $this->invitedBy ?
                    ($this->name ? "Hey, {$this->name}! " : '') . "You've been invited to join {$this->invitedBy->organization->name} on {$appName}." :
                    ($this->name ? "Hey, {$this->name}! Set up your {$appName} account." : "Get started with {$appName}.")
        );
    }
}
