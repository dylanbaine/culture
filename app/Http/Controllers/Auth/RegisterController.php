<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Organization;
use App\Models\OrganizationInvite;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Exception;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        $validation = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ];
        if (
            isset($data['organization_name'])
            && !OrganizationInvite::whereEmail($data['email'])->exists()
        ) {
            $validation['organization_name'] = ['unique:organizations,name'];
        }
        if (isset($data['organization_id'])) {
            $validation['organization_id'] = ['exists:organizations,id'];
        }
        $messages = [
            'organization_name.unique' => "The given organization name already exists. Maybe you need to be invited by an admin from your organization."
        ];
        return Validator::make($data, $validation, $messages);
    }

    protected function create(array $data)
    {
        $invite = OrganizationInvite::whereEmail($data['email'])->first();
        if ($data['organization_id'] ?? false) {
            $organization = Organization::find($data['organization_id']);
            $role = $organization->roles()->whereName('employee')->first();
        } else if (($data['organization_name'] ?? false) && !$invite) {
            $organization = $this->createOrganization($data['organization_name']);
            $role = $organization->roles()->whereName('admin')->first();
        } else {
            if (!$invite) {
                throw new Exception('You must be invited or set an organization name.');
            }
            $organization = $invite->organization;
            $role = $organization->roles()->whereName('employee')->first();
        }
        $user = User::create([
            'name' => $data['name'],
            'role_id' => $role->id,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'organization_id' => $organization->id,
        ]);
        if ($invite) {
            $invite->delete();
        }
        return $user;
    }

    protected function createOrganization(string $name): Organization
    {
        $organization = Organization::create([
            'name' => $name,
        ]);
        $organization->createDefaultRoles();
        return $organization;
    }
}
