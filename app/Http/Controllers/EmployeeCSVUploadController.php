<?php

namespace App\Http\Controllers;

use App\Imports\EmployeesImport;
use App\Jobs\BatchImportEmployees;
use App\Models\OrganizationInvite;
use Exception;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeCSVUploadController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function __invoke(Request $request)
    {
        $request->validate([
            'file' => 'file|required'
        ]);
        $csv = Excel::toArray(
            new EmployeesImport(),
            $request->file('file')->getRealPath(),
            null,
            \Maatwebsite\Excel\Excel::CSV
        );
        foreach($csv as $_csv) {
            foreach($_csv as $row) {
                if (!isset($row['email'])) {
                    session()->flash('error', 'CSV file is invalid. Make sure it has an `email` header.');
                    return redirect('/app#/employees/upload');
                }
            }
        }
        dispatch(new BatchImportEmployees($csv, user()));
        session()->flash('success', 'Invite being sent to employees.');
        return redirect('/app#/employees');
    }
    
}
