<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\StoreSurveyQuestion;
use App\Models\Survey;
use App\Models\SurveyQuestion;
use App\Models\SurveySection;

class SurveyQuestionController extends Controller
{

    public function index()
    {
        //
    }

    public function create($surveyId, $sectionId)
    {
        $survey = Survey::find($surveyId);
        $section = SurveySection::find($sectionId);
        return view('admin.surveys.questions.create', compact('survey', 'section'));
    }

    public function show($surveyId, $sectionId, $questionId)
    {
        return SurveyQuestion::find($questionId);
    }

    public function store(Request $request, $surveyId, $sectionId)
    {
        $request->validate(['question' => 'required']);
        $data = $request->all();
        $data['sectionId'] = $sectionId;
        dispatch_now(new StoreSurveyQuestion($data));
        return redirect()->route('survey-sections.show', ['surveyId' => $surveyId, 'section' => $sectionId]);
    }

    public function edit($surveyId, $sectionId, $questionId)
    {
        $question = SurveyQuestion::find($questionId);
        $survey = Survey::find($surveyId);
        $section = SurveySection::find($sectionId);
        return view('admin.surveys.questions.edit', compact('survey', 'section', 'question'));
    }

    public function update(Request $request,  $surveyId, $sectionId, $questionId)
    {
        $data = $request->all();
        $request->validate(['question' => 'required']);
        $question = SurveyQuestion::find($questionId);
        dispatch_now(new StoreSurveyQuestion($data, $question));
        session()->flash('success', 'Successfully updated');
        return back();
        return redirect()->route('survey-sections.show', ['surveyId' => $surveyId, 'section' => $sectionId]);
    }

    public function destroy($surveyId, $sectionId, $questionId)
    {
        SurveyQuestion::find($questionId)->delete();
        return back();
    }
}
