<?php

namespace App\Http\Controllers\Admin;

use App\Models\Survey;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Middleware\AdminMiddleware;
use App\Models\SurveySection;

class SurveyController extends Controller
{

    public function __constructor()
    {
        $this->middleware(AdminMiddleware::class);
    }

    public function index()
    {
        $surveys = Survey::with('sections.questions')->get();
        return view('admin.surveys.index', compact('surveys'));
    }

    public function create()
    {
        return view('admin.surveys.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $survey = Survey::create([
            'name' => $request->name,
        ]);
        return redirect()->route('surveys.show', $survey);
    }

    public function show($id)
    {
        $survey = Survey::with('sections')->find($id);
        return view('admin.surveys.show', compact('survey'));
    }

    public function edit($id)
    {
        $survey = Survey::find($id);
        return view('admin.surveys.edit', compact('survey'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $survey = Survey::Find($id);
        $survey->update([
            'name' => $request->name,
        ]);
        session()->flash('success', 'Survey updated.');
        return redirect()->route('surveys.show', $survey);
    }

    public function destroy($id)
    {
        $survey = Survey::find($id);
        $survey->sections->each(function(SurveySection $section) {
            $section->options()->delete();
            $section->questions()->delete();
        });
        $survey->sections()->delete();
        $survey->delete();
        return redirect()->route('surveys.index');
    }
}
