<?php

namespace App\Http\Controllers\Admin;

use App\Models\Survey;
use Illuminate\Http\Request;
use App\Models\SurveySection;
use App\Http\Controllers\Controller;

class SurveySectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($surveyId)
    {
        $survey = Survey::find($surveyId);
        return view('admin.surveys.sections.create', compact('survey'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $surveyId)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $section = SurveySection::create([
            'survey_id' => $surveyId,
            'name' => $request->name,
            'description' => $request->description ?? null
        ]);
        return redirect()->route('survey-sections.show', ['surveyId' => $surveyId, 'section' => $section]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($surveyId, $id)
    {
        $section = SurveySection::with('survey', 'questions')->find($id);
        return view('admin.surveys.sections.show', compact('section'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($surveyId, $id)
    {
        $survey = Survey::find($surveyId);
        $section = $survey->sections()->find($id);
        return view('admin.surveys.sections.edit', compact('survey', 'section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $surveyId, $sectionId)
    {
        $section = SurveySection::find($sectionId);
        $section->update([
            'name' => $request->name,
            'description' => $request->description ?? null
        ]);
        return redirect()->route('survey-sections.show', ['surveyId' => $surveyId, 'section' => $section]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
