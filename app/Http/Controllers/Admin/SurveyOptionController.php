<?php

namespace App\Http\Controllers\Admin;

use App\Models\Survey;
use Illuminate\Http\Request;
use App\Models\SurveySection;
use App\Http\Controllers\Controller;
use App\Models\SurveyQuestionOption;

class SurveyOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($surveyId, $sectionId)
    {
        return redirect()->route('survey-sections.show', ['surveyId' => $surveyId, 'section' => $sectionId]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($surveyId, $sectionId)
    {
        $survey = Survey::find($surveyId);
        $section = SurveySection::find($sectionId);
        return view('admin.surveys.options.create', compact('survey', 'section'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $surveyId, $sectionId)
    {
        $request->validate(['option' => 'required']);
        SurveyQuestionOption::create([
            'option' => $request->option,
            'section_id' => $sectionId
        ]);
        return redirect()->route('survey-sections.show', ['surveyId' => $surveyId, 'section' => $sectionId]);
    }

    public function edit($surveyId, $sectionId, $optionId)
    {
        $option = SurveyQuestionOption::find($optionId);
        $survey = Survey::find($surveyId);
        $section = SurveySection::find($sectionId);
        return view('admin.surveys.options.edit', compact('survey', 'section', 'option'));
    }

    public function update(Request $request,  $surveyId, $sectionId, $optionId)
    {
        $request->validate(['option' => 'required']);
        $option = SurveyQuestionOption::find($optionId);
        $option->update([
            'option' => $request->option,
        ]);
        return redirect()->route('survey-sections.show', ['surveyId' => $surveyId, 'section' => $sectionId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($surveyId, $sectionId, $optionId)
    {
        SurveyQuestionOption::find($optionId)->delete();
        return back();
    }
}
