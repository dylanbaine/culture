<?php

namespace App\Http\Controllers;

use App\Models\Survey;

class AppViewController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function welcome()
    {
        return view('welcome');
    }

    public function app()
    {
        return view('app');
    }
    
    public function takeSurvey($survey)
    {
        $survey = Survey::with('sections.questions', 'sections.options')->find($survey);
        $this->authorize('view', $survey);
        return view('take-survey', compact('survey'));
    }

    public function admin()
    {
        return view('admin');
    }

}
