<?php

namespace App\Http\Controllers;

use App\DTO\EmailLeadDTO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Actions\StoreEmailLeadAction;
use App\Mail\InviteUserToOrganization;

class GetStartedController extends Controller
{
    public function __invoke(Request $requst, StoreEmailLeadAction $storeEmailLead)
    {
        $data = $requst->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        $emailLead = new EmailLeadDTO;
        $emailLead->email = $data['email'];
        $emailLead->name = $data['name'];
        $emailLead->source = 'get started';
        $storeEmailLead->execute($emailLead);
        Mail::to($data['email'])->send(new InviteUserToOrganization(null, $data['email'], $data['name']));
        session()->flash('success', 'We just sent you an email with instructions.');
        return back();
    }
}
