<?php

use App\Models\User;
use App\Models\Organization;

function user(): ?User
{
    return Auth::user();
}

function organization(): ?Organization
{
    if ($u = user()) {
        return $u->organization;
    }
    return null;
}

function branch(): string
{
    $stringfromfile = file(app_path('../.git/HEAD'), FILE_USE_INCLUDE_PATH);
    $firstLine = $stringfromfile[0];
    $explodedstring = explode("/", $firstLine, 3);
    $branchname = $explodedstring[2];
    return trim($branchname);
}